package com.meetine.places;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MeetineApplication {

	public static void main(String[] args) {

		SpringApplication.run(MeetineApplication.class, args);
	}

}
