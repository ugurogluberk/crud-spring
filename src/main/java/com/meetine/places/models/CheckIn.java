package com.meetine.places.models;

import org.json.simple.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class CheckIn extends Users {

    private static ArrayList<CheckIn> list = new ArrayList();
    private String note;
    public static int ALL = 0;
    private int checkInType;
    private long checkInDate;

    public CheckIn(String userName, String userId, String currentBio, String note, int checkInType, int gender) {
        super(userName, userId, currentBio, gender);
        this.checkInType = checkInType;
        this.note = note;
        this.checkInDate = new Date().getTime();
        list.add(this);
    }

    public static ArrayList<CheckIn> findByID(String userId)
    {
        for(CheckIn i : CheckIn.list)
        {
            if(i.getUserId().equals(userId))
            {
                ArrayList<CheckIn> result = new ArrayList<>();
                result.add(i);
                return result;
            }
        }
        return new ArrayList<>();
    }

    public static ArrayList<CheckIn> getAll() {
        return list;
    }
}