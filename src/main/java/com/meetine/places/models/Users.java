package com.meetine.places.models;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class Users {

    private String userName, userId, currentBio;
    private int gender;
    static String[] keys = {"userId", "checkInType", "userName", "currentBio", "note", "gender", "uniId"};
    public Users(String userName, String userId, String currentBio, int gender)
    {
        this.userName = userName;
        this.userId = userId;
        this.currentBio = currentBio;
        this.gender = gender;

    }

    public static List<String> getKeys()
    {
        return Arrays.asList(Users.keys);
    }

    protected String getUserId()
    {
        return this.userId;
    }

    protected String getUserName()
    {
        return this.userName;
    }
}
