package com.meetine.places.routes;
import com.meetine.places.models.CheckIn;
import com.meetine.places.models.Users;
import org.json.simple.JSONObject;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;


@RestController
public class CheckinController {

    @PostMapping(path = "/checkin/{userid}", consumes = "application/json", produces = "application/json")
    public boolean result(@PathVariable(value = "userid") String userid, @RequestBody JSONObject body){

        boolean result = checkJSON(body);
        return result;
    }

    @GetMapping(path = "/get",   produces = "application/json")
    public ArrayList<CheckIn> get(@RequestParam int userId)
    {
        if(userId == CheckIn.ALL)
        {
            return CheckIn.getAll();
        }
        else
        {
            return CheckIn.findByID(String.valueOf(userId));
        }
    }

    boolean checkJSON(JSONObject object)
    {
        if(object.isEmpty()) return false;
        if(object.keySet().containsAll(Users.getKeys()))
            return true;
        else
            return false;
    }


}
